# Performio Systems Administrator Technical Test

We would like you to solve a series of technical challenges which cover a few points of interest. 
This is an opportunity to show off your approach to problem solving.

## Question 1
Write a python script that when executed, will output the current temperature and the time (in local timezones) in both Newport Beach, California, USA and Melbourne, Australia in a clean readable format.

## Question 2
Given the following JSON input. Write a **single line** shell command that extracts the value of **"OptionGroupName"**.

``` JSON
{
    "DBInstanceIdentifier": "rds-example",
    "DBInstanceClass": "db.m5.xlarge",
    "Engine": "mysql",
    "DBInstanceStatus": "available",
    "MultiAZ": true,
    "EngineVersion": "5.6.41",
    "AutoMinorVersionUpgrade": true,
    "ReadReplicaDBInstanceIdentifiers": [],
    "LicenseModel": "general-public-license",
    "OptionGroupMemberships": [
        {
            "OptionGroupName": "default:mysql-5-6",
            "Status": "in-sync"
        }
    ],
    "StorageType": "gp2",
    "StorageEncrypted": true,
    "DomainMemberships": [],
    "CopyTagsToSnapshot": true,
    "MonitoringInterval": 60,
    "PerformanceInsightsEnabled": true,
    "EnabledCloudwatchLogsExports": [
        "audit",
        "error",
        "general",
        "slowquery"
    ],
    "DeletionProtection": true,
    "AssociatedRoles": []
}
```
## Question 3
Create a nginx or apache web server configuration file that performs the following.

* Hosts web traffic for a website with the domain of example.com 
* Serves website data from /var/www/
* Elevates http traffic to https traffic
* Has a htaccess challenge when the /admin endpoint is requested

## Question 4
Prepare a diagram of the following AWS Environment.

* The AWS environment is a VPC Network (10.1.0.0/16) with 2 subnets. One public subnet and one private subnet.
* There is an EC2 instance in the public subnet accepting traffic from the internet on port 22, 80 and 443.
* An RDS Database instance is in the private subnet accepting traffic on port 3389 from the whole VPC Network.

Also document how you would make the environment more secure.

## Fit and Finish
1. Please submit your response in a single publically accessible git repository.

2. The git repository must have a clear and understandable README which explains your submission and provides guidence on how to run any of the applications or scripts.

3. Given more time, explain what you would do to improve your submission.